//AUTOR Drummerzzz

function clean(){        
    nome = document.getElementById("name").value ="";
    cpf = document.getElementById("cpf").value="";
    telefone = document.getElementById("telefone").value="";
    nome = document.getElementById("name").focus();
}

function updatetable(result){
    var table = document.getElementById('tcorpo');

    for(i=0; i < result.rows.length; i++){
        var list = result.rows.item(i);

        var linha = document.createElement("tr");
        var nome = document.createElement('td');
        nome.textContent = list['nome'];
        linha.appendChild(nome);

        var cpf = document.createElement('td');
        cpf.textContent = list['cpf'];
        linha.appendChild(cpf);

        var telefone = document.createElement('td');
        telefone.textContent = list['telefone'];
        linha.appendChild(telefone);
        table.appendChild(linha);
        
    }
}

var db = openDatabase("Banco","1.0","Tabalho valendo 1.5",20000);

$('body').ready(function(){
    document.getElementById("update").value="-1";
    document.getElementById("delete").value="-1";
    db.transaction(function(tx){
        tx.executeSql(
            "CREATE TABLE IF NOT EXIST cliente (nome, telefone ,cpf) ", null,
            function(tx, result){
                console.log("Tabela criada com sucesso");
            },
            function(){
                console.log("Deu merda");
            }
        )
        
        tx.executeSql(
            "SELECT * FROM cliente",null,
            function(tx, result){
                console.log('Consulta realizada com sucesso');
                var data = [];
                for(i=0; i < result.rows.length; i++){
                    var list = result.rows.item(i);
                    data[i] = list['cpf'];
                }
                updatetable(result);
            },
            function(){
                console.log("Deu merda Na consulta");
            }
        );

    });
});

function updatevalues(result){
    try{
        var list = result.rows.item(0);
        document.getElementById("name").value=list['nome'];
        document.getElementById("cpf").value=list['cpf'];
        document.getElementById("telefone").value=list['telefone'];
        document.getElementById("update").value=list['cpf'];
        document.getElementById("delete").value=list['cpf'];
    } catch (e){
        document.querySelector(".msg").innerHTML = "* Não ha registros";
    }
    
    
}

$("#search").click(function(){
    db.transaction(function(tx){
        cpf = document.getElementById("query").value;
        tx.executeSql(
            "SELECT * FROM cliente WHERE cpf = ?",[cpf],
            function(tx, result){
                console.log('Consulta de cpf realizada com sucesso');
                
                document.querySelector("#tcorpo").textContent="";
                updatetable(result);
                document.getElementById("query").value="";
                document.getElementById("query").focus();
                document.querySelector(".msg").innerHTML = "* Encontrado com Sucesso";
                updatevalues(result);
            },
            function(){
                console.log("Deu merda Na consulta do cpf");
                document.querySelector(".msg").innerHTML = "* Não ha registros";
            }
        );
    });
});

$("#list").click(function(){
    window.location.reload();
});


$("#cad").click(function(){
    nome = document.getElementById("name").value;
    cpf = document.getElementById("cpf").value;
    telefone = document.getElementById("telefone").value;
    db.transaction(function(tx){
        tx.executeSql(
            "INSERT INTO cliente (nome,cpf,telefone) VALUES (?,?,?)",[nome,cpf,telefone],
            function(tx, result){
                    console.log('INSERIDO com sucesso');
                    updatetable(result);
                    clean();
                    document.querySelector(".msg").innerHTML = "* Inserido com Sucesso";
                    document.getElementById("update").value="-1";
                    document.getElementById("delete").value="-1";
                },
            function(){
                console.log("Deu merda ao inserir");
                document.getElementById("update").value=cpf;
                document.getElementById("delete").value=cpf;
                document.querySelector(".msg").innerHTML = "* Cpf ja cadastrado! tente atualizar";
            }
        )
    });
});

$("#update")
    .click(function(){
        data = this.value;
        if(data!=="-1"){
            nome = document.getElementById("name").value;
            cpf = document.getElementById("cpf").value;
            telefone = document.getElementById("telefone").value;
            db.transaction(function(tx){
                tx.executeSql(
                    "UPDATE cliente SET nome = ?, cpf= ?, telefone =? WHERE cpf = ?",[nome,cpf,telefone,data],
                    function(tx, result){
                            console.log('Atualizado com sucesso');
                            clean();
                            document.querySelector(".msg").innerHTML = "* Atualizado com Sucesso";
                            document.getElementById('update').value="-1";
                        },
                    function(){
                        console.log("Deu merda ao Atualizar");
                        document.querySelector(".msg").innerHTML = "* Algo deu errado";
                    }
                )
        });
        }else{
            document.querySelector(".msg").innerHTML = "* Primeiro Busque um cpf";
        }
    });

$("#delete")
    .click(function(){
        data = this.value;
        if(data!=="-1"){
            db.transaction(function(tx){
                tx.executeSql(
                    "DELETE FROM cliente WHERE cpf = ?",[data],
                    function(tx, result){
                            console.log('Deletado com sucesso');
                            clean();
                            document.querySelector(".msg").innerHTML = "* Deletado com Sucesso";
                            document.getElementById('delete').value="-1";
                        },
                    function(){
                        console.log("Deu merda ao Deletar");
                        document.querySelector(".msg").innerHTML = "* Algo deu errado";
                    }
                )
        });
        }else{
            document.querySelector(".msg").innerHTML = "* Primeiro Busque um cpf";
        }
    });
